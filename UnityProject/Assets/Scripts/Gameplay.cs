﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Gameplay : MonoBehaviour 
{
	[SerializeField] private TextMesh GameText;
	[SerializeField] private Camera GameplayCamera;
	[SerializeField] private float DistancePastPlayerForDestruction = 10.0f;
	[SerializeField] private float DistanceToPlayerForLogic = 0.2f;
	[SerializeField] private float PlayerKillDistance = 10.0f;  
	[SerializeField] private float BulletKillDistance = 10.0f; 
	[SerializeField] private int MaxMissedEnemies = 3; 

	private enum State { TapToStart, Game, GameOver };

	private List<GameObject> mEnemies;
	private DifficultyCurve mDifficulty;
	private Player mPlayer;
	private float mDistance;
	private int mMissedEnemies;
	private State mCurrentState;

	public static float GameDeltaTime { get; private set; }
	public static float GameSpeed { get { return DifficultyCurve.GameSpeed; } }
	public static float PlayerSpeed { get { return DifficultyCurve.PlayerSpeed; } }
	public static float BulletSpeed { get { return DifficultyCurve.BulletSpeed; } }
	public static float ScreenBounds { get; private set; }
	public static float ScreenHeight { get; private set; }
	public static bool Paused { get; private set; }

	void Awake()
	{
		float distance = transform.position.z - GameplayCamera.transform.position.z;
		ScreenHeight = CameraUtils.FrustumHeightAtDistance( distance, GameplayCamera.fieldOfView );
		ScreenBounds = ScreenHeight * GameplayCamera.aspect * 0.5f;

		MouseInput.OnTap += HandleOnTap;
		MouseInput.OnSwipe += HandleOnSwipe;
		mEnemies = new List<GameObject>();
		mDifficulty = GetComponentInChildren<DifficultyCurve>();
		mPlayer = GetComponentInChildren<Player>();
		mCurrentState = State.TapToStart;
		mMissedEnemies = 0;
		Paused = false;
	}

	void Update()
	{
		GameDeltaTime = Paused ? 0.0f : Time.deltaTime;

		if( mCurrentState == State.Game )
		{
			mDistance += GameSpeed * GameDeltaTime;
			GameText.text = string.Format( "Distance: {0:0.0} m", mDistance );

			int enemies = mDifficulty.SpawnCount();
			if( enemies == 1 ) 
			{
				mEnemies.Add( EnemyFactory.Dispatch( Random.Range( 0, 3 ) ) );
			}
			else if( enemies == 2 )
			{
				int config = Random.Range( 0, 3 );
				if( config == 0 )
				{
					mEnemies.Add( EnemyFactory.Dispatch( 0 ) );
	             	mEnemies.Add( EnemyFactory.Dispatch( 1 ) );
				}
				else if( config == 1 )
				{
					mEnemies.Add( EnemyFactory.Dispatch( 0 ) );
					mEnemies.Add( EnemyFactory.Dispatch( 2 ) );
				}
				else 
				{
					mEnemies.Add( EnemyFactory.Dispatch( 1 ) );
					mEnemies.Add( EnemyFactory.Dispatch( 2 ) );
				}
			}
			else if( enemies == 3 )
			{
				mEnemies.Add( EnemyFactory.Dispatch( 0 ) );
				mEnemies.Add( EnemyFactory.Dispatch( 1 ) );
				mEnemies.Add( EnemyFactory.Dispatch( 2 ) );
			}

			// Update the position of each active enemy, keep a track of enemies which have gone off screen 
			List<GameObject> oldEnemys = new List<GameObject>(); 
			for( int count = 0; count < mEnemies.Count; count++ )
			{
				Vector3 position = mEnemies[count].transform.position;
				position.y -= GameDeltaTime * GameSpeed;
				mEnemies[count].transform.position = position;
				if( position.y < ScreenHeight * -0.5f )
				{
					EnemyFactory.Return( mEnemies[count] );
					oldEnemys.Add( mEnemies[count] ); 
					mMissedEnemies++;
				}
				else
				{
					Vector3 diff = mPlayer.transform.position - mEnemies[count].transform.position;
					if( diff.sqrMagnitude < PlayerKillDistance )
					{
						// Player killed - Game over
						mDifficulty.Stop();
						mCurrentState = State.GameOver;
						GameText.text = string.Format( "Crashed!\nTotal Distance: {0:0.0} m", mDistance );
					}
					else
					{
						for( int bullet = 0; bullet < mPlayer.Weapon.ActiveBullets.Count; bullet++ )
						{
							if( mPlayer.Weapon.ActiveBullets[bullet].activeInHierarchy )
							{
								Vector3 diffToBullet = mEnemies[count].transform.position - mPlayer.Weapon.ActiveBullets[bullet].transform.position;
								if( diffToBullet.sqrMagnitude < BulletKillDistance )
								{
									EnemyFactory.Return( mEnemies[count] );
									oldEnemys.Add( mEnemies[count] ); 
									mPlayer.Weapon.ActiveBullets[bullet].SetActive( false );
									break;
								}
							}
						}
					}
				}
			}

			if( mMissedEnemies >= MaxMissedEnemies )
			{
				// Player killed - Game over
				mDifficulty.Stop();
				mCurrentState = State.GameOver;
				GameText.text = string.Format( "Too many missed Enemies!\nTotal Distance: {0:0.0} m", mDistance );
			}

			for( int count = 0; count < oldEnemys.Count; count++ )
			{
				mEnemies.Remove( oldEnemys[count] );
			}
		}
	}

	private void Reset()
	{
		mPlayer.Reset();
		mDifficulty.Reset();
		EnemyFactory.Reset();
		mEnemies.Clear();
		mMissedEnemies = 0;
		mDistance = 0.0f;
	}

	private void HandleOnTap( Vector3 position )
	{
		switch( mCurrentState )
		{
		case State.TapToStart:
			Paused = false;
			mCurrentState = State.Game;
			break;
		case State.Game:
			mPlayer.Fire();
			break;
		case State.GameOver:
			Reset();
			GameText.text = "Tap to Start";
			mCurrentState = State.TapToStart;
			break;
		}
	}

	
	private void HandleOnSwipe( MouseInput.Direction direction )
	{
		if( mCurrentState == State.Game )
		{
			switch( direction )
			{
			case MouseInput.Direction.Left:
				mPlayer.MoveLeft();
				break;
			case MouseInput.Direction.Right:
				mPlayer.MoveRight();
				break;
			}
		}
	}
}
