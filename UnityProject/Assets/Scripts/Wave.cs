﻿using System;

[Serializable]
public class Wave
{
	private int mEnemiesPerRow;
	private int mRows;

	public int EnemiesPerRow { get { return mEnemiesPerRow; } }
	public int NumberOfRows { get { return mRows; } }

	public Wave( int numberOfEnemiesPerRow, int numberOfRows )
	{
		mEnemiesPerRow = numberOfEnemiesPerRow;
		mRows = numberOfRows;
	}
}
